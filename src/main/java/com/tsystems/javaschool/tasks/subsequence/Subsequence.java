package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int xid = 0;
        int yid = 0;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() > y.size()) {
            return false;
        }
        while (xid < x.size()) {
            Object xel = x.get(xid);
            Object yel = y.get(yid);
            while (!xel.equals(yel)) {
                if (++yid > y.size() - 1) {
                    return false;
                }
                yel = y.get(yid);
            }
            xid++;
        }
        return true;
    }

    public static void main(String[] args) {
        Subsequence s = new Subsequence();
        List x = Stream.of(1, 3, 5, 7, 9).collect(toList());
        List y = Stream.of(1, 2, 3, 4, 5, 7, 9, 20).collect(toList());
        boolean result = s.find(x, y);
    }
}

